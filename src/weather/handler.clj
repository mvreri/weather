(ns weather.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.json :as middleware]
            [weather.query :as query]
            [cheshire.core :as cheshire]
            [taoensso.timbre :as timbre]
            [clojure.data.json :as json]
            [clj-http.client :as httpclient]
            [weather.config :as config]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [clj-time.core :as t]
            [clj-time.periodic :refer [periodic-seq]])
  )

(defroutes app-routes


           (POST "/api/weather/weatheronline" request
             (let [body (:body request)]
               (timbre/info body)
               (timbre/info "<<<<<<<<" (:request body))

               (let  [response (httpclient/get (str "http://api.worldweatheronline.com/free/v2/weather.ashx?q=" (get-in body ["coordinates"]) "&format=json&num_of_days=" (get-in body ["days"]) " &key=8fd202cb304d99802e7132d1d7aad" )  )]


                 (timbre/info "Response from worldweather" (:body response))


                 (println "------->>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["data" "current_condition" 0 "cloudcover" ])))
                 (println "------->>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["data" "current_condition" 0 "weatherIconUrl" 0 "value" ])))
                 (println "------->>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["data" "request" 0 "query" ])))
                 (println "------->>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["data" "weather" 0 "astronomy" 0 "moonrise" ])))
                 (println "------->>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["data" "weather" 0 "date" ])))
                 (println "------->>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["data" "weather" 0 "hourly" 0 "chanceoffog" ])))
                 (println "------->>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["data" "weather" 0 "hourly" 0 "tempC" ])))
                 (println "------->>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["data" "weather" 0 "hourly" 0 "weatherDesc" 0 "value" ])))
                 (println "------->>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["data" "weather" 0 "hourly" 0 "weatherIconUrl" 0 "value" ])))
                 (println "------->>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["data" "weather" 0 "hourly" 0 "WindChillC" ])))
                 (println "------->>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["data" "weather" 0 "maxtempC"])))

                 ;(println "Name " (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["response" "venues" 0 "name"])))
                 ;(println "Twitter " (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["response" "venues" 0 "contact" "twitter"])))
                 ;(println "Address " (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["response" "venues" 0 "location" "address"])))
                 ;(println "Formatted Address " (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["response" "venues" 0 "location" "formattedaddress"])))
                 ;(println "Country Code" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["response" "venues" 0 "location" "cc"])))
                 ; (println "Distance from here: " (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["response" "venues" 0 "location" "distance"])))


                 #_(for   [n (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["response" "venues" ]))]
                   (println ";;;;;;;;;;;;;;;;;;" (:name n) "," (:twitter (:contact n)) "," (:address (:location n)) "-"(:crossStreet (:location n)) ", Distance:" (:distance (:location n))", Coordinates " (str (:lat (:location n)) "," (:lng (:location n))) ", Category" (:name (:categories n)))

                   )
                 #_(for   [q (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["response" "venues"  0 "categories"]))]
                     (println ";;;;;;;;;;;;;;;;;; , Category" (:name q))

                     )



                 )
               )
             )


           (POST "/api/weather/myweather" request
             (let [body (:body request)]
               (timbre/info body)
               (timbre/info "<<<<<<<<" (:request body))

               (let  [response (httpclient/get (str "http://api.worldweatheronline.com/free/v2/weather.ashx?q=" (get-in body ["coordinates"]) "&format=json&num_of_days=" (get-in body ["days"]) " &key=8fd202cb304d99802e7132d1d7aad" )  )]


                 (timbre/info "Response from worldweather" (:body response))


                 (println "------->>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["data" "current_condition" 0 "cloudcover" ])))
                 (println "------->>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["data" "current_condition" 0 "weatherIconUrl" 0 "value" ])))
                 (println "------->>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["data" "request" 0 "query" ])))
                 (println "------->>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["data" "weather" 0 "astronomy" 0 "moonrise" ])))
                 (println "------->>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["data" "weather" 0 "date" ])))
                 (println "------->>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["data" "weather" 0 "hourly" 0 "chanceoffog" ])))
                 (println "------->>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["data" "weather" 0 "hourly" 0 "tempC" ])))
                 (println "------->>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["data" "weather" 0 "hourly" 0 "weatherDesc" 0 "value" ])))
                 (println "------->>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["data" "weather" 0 "hourly" 0 "weatherIconUrl" 0 "value" ])))
                 (println "------->>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["data" "weather" 0 "hourly" 0 "WindChillC" ])))
                 (println "------->>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["data" "weather" 0 "maxtempC"])))

                 ;(println "Name " (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["response" "venues" 0 "name"])))
                 ;(println "Twitter " (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["response" "venues" 0 "contact" "twitter"])))
                 ;(println "Address " (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["response" "venues" 0 "location" "address"])))
                 ;(println "Formatted Address " (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["response" "venues" 0 "location" "formattedaddress"])))
                 ;(println "Country Code" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["response" "venues" 0 "location" "cc"])))
                 ; (println "Distance from here: " (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["response" "venues" 0 "location" "distance"])))


                 #_(for   [n (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["response" "venues" ]))]
                     (println ";;;;;;;;;;;;;;;;;;" (:name n) "," (:twitter (:contact n)) "," (:address (:location n)) "-"(:crossStreet (:location n)) ", Distance:" (:distance (:location n))", Coordinates " (str (:lat (:location n)) "," (:lng (:location n))) ", Category" (:name (:categories n)))

                     )
                 #_(for   [q (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["response" "venues"  0 "categories"]))]
                     (println ";;;;;;;;;;;;;;;;;; , Category" (:name q))

                     )



                 )
               )
             )





           ;populate targetlist for already existing campaigns(on subscriber creation)
           #_(POST "/api/targetlist/populate/existing" request

               (let [body (:body request)]
                 (timbre/info body)
                 (timbre/info "<<<<<<<<" (:request body))

                 (println "Posted MSISDN___"  (get-in body ["msisdn"]))
                 (println "Posted CampaignID___"  (get-in body ["campaignid"]))

                 (if (= (get-in body ["msisdn"]) nil)
                   (json/write-str {:err-msg "No MSISDN specified" :status 1 :user-data "Error 400" })
                   (if (= (get-in body ["campaignid"]) nil)
                     (json/write-str {:err-msg "No Campaign ID specified" :status 1 :user-data "Error 400" })
                     (if (not (number? (get-in body ["msisdn"])))
                       (json/write-str {:err-msg "Invalid msisdn" :status 1 :user-data "Error 430" })
                       (let [msisdns (query/populate-targetlist-existing body)]
                         (json/write-str {:err-msg "success" :status 0 :user-data msisdns })
                         )

                       )
                     )
                   )


                 #_(if (= (get-in body ["msisdn"]) nil)
                     (json/write-str {:err-msg "No MSISDN specified" :status 1 :user-data "Error 400" })
                     (let [msisdns (query/get-targetlist body)]
                       (json/write-str {:err-msg "success" :status 0 :user-data msisdns })
                       )
                     )


                 ))



           #_(POST "/api/campaigns" request

               (let [body (:body request)]
                 (timbre/info body)
                 (timbre/info "<<<<<<<<" (:request body))


                 (do
                   (let [campaigns (query/add-campaign body)]
                     (json/write-str {:err-msg "campaign added successfully" :status 0 :user-data campaigns }))


                   ;generating a target list
                   (let  [jsonstrb
                          (json/write-str
                            {
                             :county (get-in body ["location_"])
                             :gender (get-in body ["gender"])
                             ;:status (get-in body ["status"])
                             :occupation (get-in body ["occupation"])
                             })
                          response (httpclient/post (str config/SIKIKA-SUBSCRIBER-BASE-URL "mode/sikika/subscriber-info/" ) {:form-params {:username (str "sikika") :password (str "sikika123") :lowerage (get-in body ["age_lower"])  :upperage (get-in body ["age_upper"]) :sub-info jsonstrb}} )]


                     (timbre/info "Response from subscribers module" (:body response) )


                     (println "_____________" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["status-msg" 0 "msisdn"])))
                     (println "_____________2___" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["status-msg" 0 ])))
                     (println "_____________2___" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["status"])))
                     (println "_____________3___" (:body response))

                     (if (= (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["status"])) "failed")
                       (json/write-str {:err-msg "fail" :status 1 :user-data nil })
                       (do
                         (doseq [e (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["status-msg"])) ]
                           (println "!!_______________"  (:msisdn e))
                           (let [jsontarget (json/write-str
                                              {
                                               :msisdn(:msisdn e)
                                               })
                                 responsetarget (:body (httpclient/post (str config/SIKIKA-CAMPAIGN-MANAGEMENT-BASE-URL "api/targetlist/populate") {:body jsontarget :body-encoding "UTF-8" :content-type :json}))]
                             (timbre/info "Targetlist updated--->>>>>>>>" jsontarget)
                             (timbre/info "response after call------><<<<<<" responsetarget)
                             responsetarget
                             )
                           )
                         (json/write-str {:err-msg "success" :status 0 :user-data nil })
                         )
                       )



                     )
                   )

                 )
               )

           ;EDITING the campaign
           #_(PUT "/api/campaigns/:id" request
               (let [body (:body request)]
                 (timbre/info body)
                 (let [scheme (:params request)]
                   (let [id (:id scheme)]
                     (println (get-in body ["name"]))
                     (let [id id]

                       {:status 200
                        :content-type "application/json; charset=UTF-8"
                        :body (cheshire/generate-string (query/update-campaign (Integer/parseInt id) body))}
                       )
                     )
                   )
                 )
               )



           ;remove campaign from DB. May note be necessary. USE THE SUSPEND/APPROVE ONES INSTEAD!
           #_(DELETE "/api/campaigns/:id" [id]
               {:status 200
                :content-type "application/json; charset=UTF-8"
                :body (cheshire/generate-string (query/delete-campaign (Integer/parseInt id)))}
               )




           (route/resources "/")
           (route/not-found "Not Found"))

(def app
  (-> app-routes
      (middleware/wrap-json-body)
      (middleware/wrap-json-response)
      (wrap-defaults app-routes)))
