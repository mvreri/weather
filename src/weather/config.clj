(ns weather.config
  (:require [clojure.tools.logging :as log]
            )
  (:import  [java.util Properties]
            [java.io FileInputStream]
            )
  (:use [clojure.walk])
  (:use korma.core)
  (:use korma.db))

(def app-configs (atom {}))

(defn- config-file []
  (let [result (let [result (System/getenv "sikika")]
                 (when result (java.io.File. result)))]
    (if (and result (.isFile result))
      result
      (do (log/fatal (format "serverConfig(%s) = nil" result))
          (throw (Exception. (format "Server configuration file (%s) not found." result)))))))

(defn load-config [config]
  (let [properties (Properties.)
        fis (FileInputStream. config)]
    ; populate the properties hashmap with values from the output stream
    (.. properties (load fis))
    (keywordize-keys (into {} properties))))

(defn config-value [name & args]
  (let [value (@app-configs name)]
    (when value
      (let [args (when args (apply assoc {} args))
            {type :type} args
            args (dissoc args :type)
            value (if (vector? value)
                    (loop [x (first value)
                           xs (next value)]
                      (let [properties (dissoc x :value)]
                        (if (or (and (empty? args)
                                     (empty? properties))
                                (and (not (empty? args))
                                     (every? (fn [[k v]]
                                               (= (properties k) v))
                                             args)))
                          (x :value)
                          (when xs
                            (recur (first xs) (next xs))))))
                    value)]
        (when value
          (let [value #^String value]
            (cond (or (nil? type) (= type :string)) value
                  ;; ---
                  (= type :int) (Integer/valueOf value)
                  (= type :long) (Long/valueOf value)
                  (= type :bool) (contains? #{"yes" "true" "y" "t" "1"}
                                            (.toLowerCase value))
                  (= type :path) (java.io.File. value)
                  (= type :url) (java.net.URL. value)
                  )))))))

(defn initialize-config []
  (log/info "initializing configurations..")
  (reset! app-configs (load-config (config-file)))

  ;;sikika database
  (def sikika-db-host (config-value :sikika-db-host))
  (def sikika-db-port (config-value :sikika-db-port))
  (def sikika-db-name (config-value :sikika-db-name))
  (def sikika-db-user (config-value :sikika-db-user))
  (def sikika-db-pass (config-value :sikika-db-password))
  (def sikika-db-subprotocol (config-value :sikika-db-subprotocol))
  (def sikika-db-subname (config-value :sikika-db-subname))


  (def sikika-cdr-db-host (config-value :sikika-cdr-db-host))
  (def sikika-cdr-db-port (config-value :sikika-cdr-db-port))
  (def sikika-cdr-db-name (config-value :sikika-cdr-db-name))
  (def sikika-cdr-db-user (config-value :sikika-cdr-db-user))
  (def sikika-cdr-db-pass (config-value :sikika-cdr-db-password))
  (def sikika-cdr-db-subprotocol (config-value :sikika-cdr-db-subprotocol))
  (def sikika-cdr-db-subname (config-value :sikika-cdr-db-subname))

  ;this is the url that the registration moduke will be hosted on. Other modules, as well as this one, will reference this variable
  (def SIKIKA-CAMPAIGN-MANAGEMENT-BASE-URL (config-value :SIKIKA-CAMPAIGN-MANAGEMENT-BASE-URL))
  ;;the url that is used to post/fetch data from and to the subscribers module
  (def SIKIKA-SUBSCRIBER-BASE-URL (config-value :SIKIKA-SUBSCRIBER-BASE-URL))

  (def SIKIKA-BENEFITS-BASE-URL (config-value :SIKIKA-BENEFITS-BASE-URL))


  (def ISO-CODE (config-value :ISO-CODE))
  (def BENEFITS-UNAME (config-value :BENEFITS-UNAME))
  (def BENEFITS-PASS (config-value :BENEFITS-PASS))
  (def ADDITIONAL-CASH-TO-CREDIT (config-value :ADDITIONAL-CASH-TO-CREDIT))


  (println "user --> "sikika-db-user)
  (println "pas --> "sikika-db-pass)
  (println "subp --> "sikika-db-subprotocol)
  (println "subn --> "sikika-db-subname)
  (println "management URL --> "SIKIKA-CAMPAIGN-MANAGEMENT-BASE-URL)
  (println "subscriber URL --> "SIKIKA-SUBSCRIBER-BASE-URL)
  (println "Benefits/Rewards URL --> "SIKIKA-BENEFITS-BASE-URL)
  (println "iso code --> "ISO-CODE)
  (println "benefits uname --> "BENEFITS-UNAME)
  (println "benefits password --> "BENEFITS-PASS)
  (println "additional cash to credit --> "ADDITIONAL-CASH-TO-CREDIT)

  )


(defn initialize-db []
  (initialize-config)
  (defdb sikika (postgres {:db sikika-db-name
                           :user sikika-db-user
                           :password sikika-db-pass
                           :host sikika-db-host
                           :port sikika-db-port
                           :delimiters ""}))

  )
