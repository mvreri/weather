(ns weather.database
  (:require [korma.db :as korma]
            [lobos.connectivity :as lobos]
            [weather.config :as config]))

(config/initialize-db)

#_(def db-connection-info
    {:classname "org.postgresql.Driver"
     :subprotocol "postgresql"
     :user "postgres"
     :password "postgres"
     :subname "//localhost:5432/sikika"})

(def db-connection-info
  {:classname "org.postgresql.Driver"
   :subprotocol config/sikika-db-subprotocol
   :user config/sikika-db-user
   :password config/sikika-db-pass
   :subname config/sikika-db-subname})

; set up korma
(korma/defdb db db-connection-info)
; set up lobos
(lobos/open-global db-connection-info)