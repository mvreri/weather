(ns weather.query
  (:require [weather.database]
            [taoensso.timbre :as timbre]
            [clojure.data.json :as json]
            [korma.core :refer :all]
            [weather.config :as config]
            [clojure.string :as cstr]
            [korma.db :refer :all]
            [clj-http.client :as httpclient]))

(def db-connection-info
  {:classname "org.postgresql.Driver"
   :subprotocol config/sikika-db-subprotocol
   :user config/sikika-db-user
   :password config/sikika-db-pass
   :subname config/sikika-db-subname
   })

(def db-connection-info-b
  {:classname "org.postgresql.Driver"
   :subprotocol config/sikika-cdr-db-subprotocol
   :user config/sikika-cdr-db-user
   :password config/sikika-cdr-db-pass
   :subname config/sikika-cdr-db-subname})

(defn get-campaigns []
  (with-db db-connection-info (exec-raw (format "SELECT id id, name cname, location_ as location, gender gender, status status, age_lower age_lower, age_upper age_upper,
                                                  isactive approved, cmpn_datefrom campaign_start, cmpn_dateto campaign_stop , content_ as files,
                                                  CASE  when t1a like t1b then 'Invalid campaign schedule'
                                                  WHEN (t1a IS NULL OR t1b IS NULL) then 'Invalid campaign schedule(nulls)'
                                                  end schedulevalidity,
                                                  t1a as timeslot1_start, t1b as timeslot1_stop,
                                                  t2a as timeslot2_start, t2b as timeslot2_stop,
                                                  t3a as timeslot3_start, t3b as timeslot3_stop
                                                  FROM tbl_campaign_") :results))
  )

(def alphanumeric "1234567890")
(defn get-random-id [length]
  (loop [acc []]
    (if (= (count acc) length) (apply str acc)
                               (recur (conj acc (rand-nth alphanumeric))))))



(defn update-targetlist-from-cdrs []

  (do

    (let [newcompletes (with-db db-connection-info-b (exec-raw  (format  "SELECT  trim(leading '+' from  src) srca, src, dst,  dcontext,  disposition,  hangupcode,  completed,  mediacdrs,  campaignid,  starttime,  endtime,  billsec
                                                                        FROM asterisk_cdr b
                                                                        WHERE NOT EXISTS (
                                                                            SELECT *
                                                                                FROM tbl_asterisk_cdr a
                                                                                    WHERE a.dst = b.dst
                                                                                    AND a.src = b.src
                                                                                        AND a.dcontext = b.dcontext
                                                                                            AND a.disposition = b.disposition
                                                                                                AND a.hangupcode = b.hangupcode
                                                                                                    AND a.completed = b.completed
                                                                                                        AND a.mediacdrs = b.mediacdrs
                                                                                                            AND a.campaignid = b.campaignid
                                                                                                              AND a.starttime=b.starttime
                                                                                                                AND a.endtime=b.endtime
                                                                                                                        AND a.billsec = b.billsec    )
                                                                                                                            AND b.completed=1 AND disposition='ANSWERED';") :results)
                                )

          ]


      ;get the CDRs that have not yet been updated on the local cdr table
      (doseq [w newcompletes]
        (println "==================================")
        (println w "{{" (:srca w))
        (println w "->" (:campaignid w))


        ;split the campaign ids on pipe as in the records received
        (if (< (count (cstr/split (:campaignid w) #"\|" -1)) 2)




          (do
            (let [responze (httpclient/post (str  config/SIKIKA-BENEFITS-BASE-URL "mode/bluepodapi/v1/credit/" ) {:query-params {:MSISDN (str "254" (:srca w))
                                                                                                                                 :OriginTransactionID (str (get-random-id 5))
                                                                                                                                 :Reward (str (if (< (+ (quot (:billsec w) 15) (read-string config/ADDITIONAL-CASH-TO-CREDIT)) 5) 5 (+ (quot (:billsec w) 15) (read-string config/ADDITIONAL-CASH-TO-CREDIT))))
                                                                                                                                 :ISOCurrCode (str config/ISO-CODE)
                                                                                                                                 :User (str config/BENEFITS-UNAME)
                                                                                                                                 :Password (str config/BENEFITS-PASS)
                                                                                                                                 }
                                                                                                                  :body-encoding "UTF-8"
                                                                                                                  :content-type "text/plain"
                                                                                                                  }
                                            )]
              responze
              (println "<<<========>>>>>" responze)
              (timbre/info "<<<===Message from ETOPUP=====>>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body responze) ) ["Msg"])))
              (timbre/info "<<<===Status=====>>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body responze) ) ["Status"])))
              )
            (with-db db-connection-info (exec-raw  (format  "UPDATE tbl_targetlist SET listens=listens+1 WHERE campaignid=%s AND msisdn='%s';" (:campaignid w) (:srca w))))
            )



          (doseq [e (cstr/split (:campaignid w) #"\|" -1)]
            (timbre/info "Campaign ID has more than  one campaign " e)
            (println e "+" (:src w) )
            (with-db db-connection-info (exec-raw  (format  "UPDATE tbl_targetlist SET listens=listens+1 WHERE campaignid=%s AND msisdn='%s';" (Integer/parseInt e) (:srca w))))


            #_(println (:dst w) ">>"(quot (:billsec w) 15) ">>" (get-random-id 5) ">>" (+ (quot (:billsec w) 15) (read-string config/ADDITIONAL-CASH-TO-CREDIT)))

            (if (< (/ (+ (quot (:billsec w) 15) (read-string config/ADDITIONAL-CASH-TO-CREDIT)) (count (cstr/split (:campaignid w) #"\|" -1)) ) 5)
              (let [responze (httpclient/post (str  config/SIKIKA-BENEFITS-BASE-URL "mode/bluepodapi/v1/credit/" ) {:query-params {:MSISDN (str "254"(:srca w))
                                                                                                                                   :OriginTransactionID (str (get-random-id 5))
                                                                                                                                   :Reward (str 5)
                                                                                                                                   :ISOCurrCode (str config/ISO-CODE)
                                                                                                                                   :User (str config/BENEFITS-UNAME)
                                                                                                                                   :Password (str config/BENEFITS-PASS)
                                                                                                                                   }
                                                                                                                    :body-encoding "UTF-8"
                                                                                                                    :content-type "text/plain"
                                                                                                                    }
                                              )]
                responze
                (println "<<<========>>>>>" responze)
                (timbre/info "<<<===Message from ETOPUP=====>>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body responze) ) ["Msg"])))
                (timbre/info "<<<===Status=====>>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body responze) ) ["Status"])))
                )
              (let [responze (httpclient/post (str  config/SIKIKA-BENEFITS-BASE-URL "mode/bluepodapi/v1/credit/" ) {:query-params {:MSISDN (str "254"(:srca w))
                                                                                                                                   :OriginTransactionID (str (get-random-id 5))
                                                                                                                                   :Reward (str (/ (+ (quot (:billsec w) 15) (read-string config/ADDITIONAL-CASH-TO-CREDIT)) (count (cstr/split (:campaignid w) #"\|" -1)) ))
                                                                                                                                   :ISOCurrCode (str config/ISO-CODE)
                                                                                                                                   :User (str config/BENEFITS-UNAME)
                                                                                                                                   :Password (str config/BENEFITS-PASS)
                                                                                                                                   }
                                                                                                                    :body-encoding "UTF-8"
                                                                                                                    :content-type "text/plain"
                                                                                                                    }
                                              )]
                responze
                (println "<<<========>>>>>" responze)
                (timbre/info "<<<===Message from ETOPUP=====>>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body responze) ) ["Msg"])))
                (timbre/info "<<<===Status=====>>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body responze) ) ["Status"])))
                ))

            (println "````````````````````" (/ (+ (quot (:billsec w) 15) (count (cstr/split (:campaignid w) #"\|" -1))) (read-string config/ADDITIONAL-CASH-TO-CREDIT)))


            )
          )





        )
      ;update the targetlist from original CDR table
      #_(let [campaignids (with-db db-connection-info (exec-raw  (format  "select campaignid campaignid, dst msisdn
                                                                      FROM asterisk_cdr
                                                                      WHERE NOT EXISTS (
                                                                      SELECT 'X' FROM  tbl_asterisk_cdr
                                                                      WHERE
                                                                      asterisk_cdr.dst= tbl_asterisk_cdr.dst AND
                                                                      asterisk_cdr.dcontext=tbl_asterisk_cdr.dcontext AND
                                                                      asterisk_cdr.disposition=tbl_asterisk_cdr.disposition AND
                                                                      asterisk_cdr.hangupcode=tbl_asterisk_cdr.hangupcode AND
                                                                      asterisk_cdr.completed=tbl_asterisk_cdr.completed AND
                                                                      asterisk_cdr.mediacdrs=tbl_asterisk_cdr.mediacdrs AND
                                                                      asterisk_cdr.campaignid=tbl_asterisk_cdr.campaignid)
                                                                      AND asterisk_cdr.disposition='ANSWERED' OR asterisk_cdr.completed=1;") :results)
                                   )]

          #_(println "<<<<<<<<<<<<<<<<" (first campaignids))
          #_(println "<<<<<<<<<<<<<<<<" (:campaignid (first campaignids)))
          #_(println "<<<<<<<<<<<<<<<<>>>>>>>>" (:msisdn (first campaignids)))
          #_(println ">>>>>>>>>>>>>>>" (cstr/split (:campaignid (first campaignids)) #"\|"))



          ;get the individual ids from each campaign played to update listens on targetlist
          ;splitting on pipe
          (doseq [e (cstr/split (:campaignid (first campaignids)) #"\|" -1)]
            #_(println e "+" (:msisdn (first campaignids)))
            #_(timbre/info "Updated Info for Campaign" e "MSISDN: "(:msisdn (first campaignids)))
            ;update ttargetlist
            #_(println "---> "(with-db db-connection-info (exec-raw  (format  "SELECT msisdn, listens FROM tbl_targetlist WHERE campaignid=%s AND msisdn='%s';" (Integer/parseInt e) (:msisdn (first campaignids))) :results)))
            (with-db db-connection-info (exec-raw  (format  "UPDATE tbl_targetlist SET listens=listens+1 WHERE campaignid=%s AND msisdn='%s';" (Integer/parseInt e) (:msisdn (first campaignids)))))
            #_(println "Updated " e "+" (:msisdn (first campaignids)))
            (timbre/info "Updated Info for Campaign" e "MSISDN: "(:msisdn (first campaignids)))
            )
          )


      ;update the replica cdr table
      #_(with-db db-connection-info (exec-raw (format  "INSERT INTO tbl_asterisk_cdr( dst,  dcontext,  disposition,  hangupcode,  completed,  mediacdrs,  campaignid,  starttime,  endtime,  billsec)
                                                  select dst,  dcontext,  disposition,  hangupcode,  completed,  mediacdrs,  campaignid,  starttime,  endtime,  billsec
                                                  FROM asterisk_cdr
                                                  WHERE NOT EXISTS (
                                                    SELECT 'X' FROM  tbl_asterisk_cdr
                                                    WHERE
                                                     asterisk_cdr.dst= tbl_asterisk_cdr.dst AND
                                                      asterisk_cdr.dcontext=tbl_asterisk_cdr.dcontext AND
                                                       asterisk_cdr.disposition=tbl_asterisk_cdr.disposition AND
                                                        asterisk_cdr.hangupcode=tbl_asterisk_cdr.hangupcode AND
                                                         asterisk_cdr.completed=tbl_asterisk_cdr.completed AND
                                                          asterisk_cdr.mediacdrs=tbl_asterisk_cdr.mediacdrs AND
                                                           asterisk_cdr.campaignid=tbl_asterisk_cdr.campaignid  );") )
                 )

      (with-db db-connection-info-b (exec-raw  (format  "INSERT INTO tbl_asterisk_cdr( src, dst,  dcontext,  disposition,  hangupcode,  completed,  mediacdrs,  campaignid,  starttime,  endtime,  billsec)
                                                      SELECT  src, dst,  dcontext,  disposition,  hangupcode,  completed,  mediacdrs,  campaignid,  starttime,  endtime,  billsec
                                                                        FROM asterisk_cdr b
                                                                        WHERE NOT EXISTS (
                                                                            SELECT *
                                                                                FROM tbl_asterisk_cdr a
                                                                                    WHERE a.dst = b.dst
                                                                                     AND a.dst = b.dst
                                                                                        AND a.dcontext = b.dcontext
                                                                                            AND a.disposition = b.disposition
                                                                                                AND a.hangupcode = b.hangupcode
                                                                                                    AND a.completed = b.completed
                                                                                                        AND a.mediacdrs = b.mediacdrs
                                                                                                            AND a.campaignid = b.campaignid
                                                                                                            AND a.starttime=b.starttime
                                                                                                               AND a.endtime=b.endtime
                                                                                                                        AND a.billsec = b.billsec    )
                                                                                                                            AND b.completed=1 AND disposition='ANSWERED';") )
               )
      )
    (timbre/info "UPDATED CDRS________CRON")

    )
  )

(defn get-ad-pricing []
  (with-db db-connection-info (exec-raw (format "SELECT id, amount_paid FROM tbl_campaign") :results)))

;display the whole targetlist
(defn get-whole-targetlist []
  "give the list of the msisdns and the ads associated"
  (with-db db-connection-info (exec-raw (format "SELECT tl.id id, campaignid campaignid, tl.msisdn msisdn, tl.listens listens, c.name cname FROM tbl_targetlist tl
                                                  INNER JOIN tbl_campaign_ c
                                                  ON c.id = tl.campaignid;") :results))
  )

;;getting the campaigns associated to a given msisdn supplied by the user
(defn get-targetlist [body]
  "specific msisdns"
  (with-db db-connection-info (exec-raw (format "SELECT c.id campaignid, tl.msisdn msisdn, tl.listens listens, c.content_ files, budget campaigncost FROM tbl_targetlist tl
                                                 INNER JOIN tbl_campaign_ c
                                                 ON c.id = tl.campaignid
                                                 WHERE listens <=2
                                                 AND cast (tl.msisdn as real)='%s'" (get-in body ["msisdn"])) :results))
  )

(defn add-campaign [body]
  (timbre/info "at the query stage" body )
  (with-db db-connection-info (exec-raw (format "INSERT INTO tbl_campaign_ (name, age_upper, age_lower, gender, location_, status, occupation, cmpn_reach, cmpn_datefrom, cmpn_dateto, budget, content_, cmpn_frequency, t1a, t1b, t2a, t2b, t3a, t3b, advertiserid, postedstring)
                                              VALUES('%s', %s, %s, '%s', '%s', '%s', '%s', %s, '%s', '%s', %s, '%s', %s, '%s', '%s', '%s', '%s', '%s', '%s', %s, '%s')"
                                                (get-in body ["name"]) (get-in body ["age_lower"]) (get-in body ["age_upper"]) (get-in body ["gender"]) (get-in body ["location_"])
                                                (get-in body ["status"]) (get-in body ["occupation"]) (get-in body ["cmpn_cmpn_reach"]) (get-in body ["cmpn_datefrom"])
                                                (get-in body ["cmpn_dateto"]) (get-in body ["budget"]) (get-in body ["content_"]) (get-in body ["cmpn_frequency"])
                                                (get-in body ["t1a"])(get-in body ["t1b"])(get-in body ["t2a"])(get-in body ["t2b"])(get-in body ["t3a"])(get-in body ["t3b"])
                                                (get-in body ["advertiserid"]) (json/write-str body))))

  )



(defn get-subs []
  (with-db db-connection-info (exec-raw (format "SELECT id, msisdn FROM tbl_users") :results)))

(defn populate-targetlist [response]
  ;(exec-raw (format "INSERT INTO tbl_targetlist(msisdn,campaignid)VALUES('%s',(SELECT MAX(id) FROM tbl_campaign_)) WHERE NOT EXISTS (SELECT );" (get-in response ["msisdn"]) ) )
  (with-db db-connection-info  (exec-raw (format "INSERT INTO tbl_targetlist(msisdn,campaignid)
                                                SELECT '%s',(SELECT MAX(id) FROM tbl_campaign_)
                                                WHERE NOT EXISTS (SELECT msisdn from tbl_targetlist
                                                where  msisdn='%s' AND campaignid=(SELECT MAX(id) FROM tbl_campaign_));"
                                                 (get-in response ["msisdn"]) (get-in response ["msisdn"])) ))

  )

(defn populate-targetlist-existing [response]
  ;(exec-raw (format "INSERT INTO tbl_targetlist(msisdn,campaignid)VALUES('%s',(SELECT MAX(id) FROM tbl_campaign_)) WHERE NOT EXISTS (SELECT );" (get-in response ["msisdn"]) ) )
  (with-db db-connection-info (exec-raw (format "INSERT INTO tbl_targetlist(msisdn,campaignid)
                      VALUES ('%s','%s');" (get-in response ["msisdn"]) (get-in response ["campaignid"])) ))

  )




(defn get-ad-msisdns []
  (with-db db-connection-info (exec-raw (format "SELECT cl.campaign_id, cl.msisdn FROM tbl_usercampaignlist cl
                                                INNER JOIN tbl_campaign c
                                                ON c.id = cl.campaign_id::bigint
                                                WHERE c.cmpn_reach<c.cmpn_reachtarget
                                                AND c.cmpn_frequency > (extract(epoch from current_timestamp - cl.timeupdated))") :results))
  )




(defn approve-campaign [body]
  (exec-raw (format "UPDATE tbl_campaign_ SET isactive=1 WHERE id=%s;" (get-in body ["id"])) ))


(defn suspend-campaign [body]
  (exec-raw (format "UPDATE tbl_campaign_ SET isactive=2 WHERE id=%s;" (get-in body ["id"])) ))

#_(defn update-campaign [id body]
    (update tbl_campaign
            (set-fields {:content_ (get-in body ["content"])
                         :religion_christian (get-in body ["christian"])
                         :religion_islam (get-in body ["religion_islam"])
                         :religion_hindu (get-in body ["religion_hindu"])
                         :religion_other (get-in body ["religion_other"])
                         :age_lower (get-in body ["age_lower"])
                         :age_upper (get-in body ["age_upper"])
                         :gender_male (get-in body ["gender_male"])
                         :gender_female (get-in body ["gender_female"])
                         :location_ (get-in body ["location"])
                         :marital_single (get-in body [" marital_single"])
                         :marital_married (get-in body ["marital_married"])
                         :marital_divorced (get-in body ["marital_divorced"])
                         :marital_widowed (get-in body ["marital_widowed"])
                         :occupation_student (get-in body ["occupation_student"])
                         :occupation_unemployed (get-in body ["occupation_unemployed"])
                         :occupation_employed (get-in body ["occupation_employed"])
                         :occupation_selfemployed (get-in body ["occupation_selfemployed"])
                         :cmpn_timing (get-in body ["cmpn_timing"])
                         :cmpn_timecycle (get-in body ["cmpn_timecycle"])
                         :cmpn_reachtarget (get-in body ["cmpn_reachtarget"])
                         :cmpn_frequency (get-in body ["cmpn_frequency"])
                         :cmpn_reward (get-in body ["cmpn_reward"])
                         }
                        )
            (where {:id [= id]})
            )
    )




(defn get-campaign [id]
  (with-db db-connection-info (exec-raw (format "SELECT id id, name cname, location_ as location, gender gender, status status, age_lower age_lower, age_upper age_upper,
                                                  isactive approved, cmpn_datefrom campaign_start, cmpn_dateto campaign_stop , content_ as files,
                                                  CASE  when t1a like t1b then 'Invalid campaign schedule'
                                                  WHEN (t1a IS NULL OR t1b IS NULL) then 'Invalid campaign schedule(nulls)'
                                                  end schedulevalidity,
                                                  t1a as timeslot1_start, t1b as timeslot1_stop,
                                                  t2a as timeslot2_start, t2b as timeslot2_stop,
                                                  t3a as timeslot3_start, t3b as timeslot3_stop
                                                  FROM tbl_campaign_
                                                  WHERE id=%s;" id) :results))
  )
