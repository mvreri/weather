(defproject weather "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [compojure "1.4.0"]
                 [ring/ring-defaults "0.1.5"]
                 [ring/ring-json "0.2.0"]
                 [korma "0.4.0"]
                 [org.postgresql/postgresql "9.3-1102-jdbc41"]
                 [lobos "1.0.0-beta3"]
                 [cheshire "5.5.0"]
                 [com.taoensso/timbre "3.3.1"]
                 [org.clojure/data.json "0.2.5"]
                 [org.clojure/tools.logging "0.2.6"]
                 [org.clojure/java.jdbc "0.3.6"]
                 [clj-http "1.0.1"]]
  :plugins [[lein-ring "0.9.7"]]
  :ring {:handler weather.handler/app}
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring/ring-mock "0.3.0"]]}})
